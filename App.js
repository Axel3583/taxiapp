import { NavigationContainer } from '@react-navigation/native';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginScreen from './src/screens/LoginScreen';
import HomeScreen from './src/screens/HomeScreen';
import * as Font from 'expo-font';
import { useEffect, useState } from 'react';
import { renderInitialScreen } from './utils/prefix';
import * as Permissions from 'expo-permissions';
import PassengerScreen from './src/screens/PassengerScreen';
import DriverScreen from './src/screens/DriverScreen';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FoodScreen from './src/screens/FoodScreen';


const Drawer = createDrawerNavigator();
const Tab = createMaterialBottomTabNavigator();



export default function App() {

  useEffect(() => {
    loadRessources()
  }, [])

  const [loader, setloader] = useState(true)
  const [initialScreen, setInitialScreen] = useState()

  const loadRessources = async () => {
    try {
      const result = await Promise.all([
        await Font.loadAsync({
          'poppins': require("./assets/9/fonts/Poppins-Regular.ttf"),
          'LeckerliOne': require("./assets/9/fonts/LeckerliOne-Regular.ttf"),
        }),
        renderInitialScreen(),
        Permissions.askAsync(Permissions.LOCATION_BACKGROUND),
      ]);

      const route = result[1];
      const status = "granted";
      if (route && status === "granted") {
        setInitialScreen(route)
        setloader(false)
      }
    } catch (error) {
      console.error('ressources indisponible :', error);
    }
  }

  if (loader) {
    return (
      <View style={styles.text}>
        <ActivityIndicator />
      </View>
    )
  }
  const { Navigator, Screen } = createNativeStackNavigator()
  return (

    <NavigationContainer>

      {/* <Drawer.Navigator>
        <Drawer.Screen name="Login" component={LoginScreen} />
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Passenger" component={PassengerScreen} />
        <Drawer.Screen name="Driver" component={DriverScreen} />
      </Drawer.Navigator> */}

      <Tab.Navigator
        initialRouteName="Login"
        activeColor="#f0edf6"
        inactiveColor="#3e2465"
        barStyle={{
          backgroundColor: "#2dbb54",
          shadowColor: "#000",
          shadowRadius: 5,
          shadowOpacity: 0.25
        }}
      >
        <Tab.Screen
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="home" color={'white'} size={26} />
            ),
          }} name="Home" component={HomeScreen} />
        <Tab.Screen
          options={{
            tabBarLabel: 'Food',
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="food" color={'white'} size={26} />
            ),
          }} name="Food" component={FoodScreen} />
        <Tab.Screen name="Passenger"
          options={{
            tabBarLabel: 'Passenger',
            tabBarLabelStyle: { color: 'blue' }
          }} component={PassengerScreen} />
        <Tab.Screen
          options={{
            tabBarLabel: 'Driver',
            tabBarLabelStyle: { color: 'blue' },
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="car" color={'white'} size={26} />
            ),
          }} name="Driver" component={DriverScreen} />
      </Tab.Navigator>

      {/* <Navigator initialRouteName={initialScreen} screenOptions={{ headerShown: false }}>
        <Screen name='Login' component={LoginScreen} />
        <Screen name='Home' component={HomeScreen} />
        <Screen name='Passenger' component={PassengerScreen} />
        <Screen name='Driver' component={DriverScreen} />
      </Navigator> */}

    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    flex: 1,
    backgroundColor: '#f0f8ff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
