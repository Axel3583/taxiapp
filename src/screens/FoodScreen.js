import React from "react";
import { StyleSheet, View, Text} from "react-native";


export default function FoodScreen(){

   return(
    <View style={styles.container}>
        <Text>FoodScreen</Text>
    </View>
   )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }
})