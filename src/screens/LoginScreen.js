import React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import Block from '../components/Block';
import Title from '../components/Title';
import { Ionicons } from '@expo/vector-icons';
import { prefix, useGoogleAuth } from '../../utils/prefix';
import LoginBtn from '../components/LoginBtn';



export default function LoginScreen({navigation}) {

  // const { request, response, promptAsync } = useGoogleAuth();

  // const handleAuth = async () => {
  //     try {
  //         // promptAsync will open the Google authentication screen
  //         await promptAsync();
  //         console.log('result', response);
  //     } catch (error) {
  //         console.error('error Auth :', error);
  //     }
  // }

  const handleAuth = () => {
  console.log('Connecté...');
  navigation.navigate('Home');
  }

  
  return (
    <View style={styles.container}>
      <Block>
        <Ionicons name={`${prefix}-car-sport-outline`} style={styles.icons} />
        <Title size={'big'} content="TAXI APP" />
      </Block>

      <View style={styles.container_2}>
        <View style={styles.titleContainer}>
          <Title size={'small'} content="authentification" />
          <Title size={'medium'} content="GOOGLE CONNEXION" />
        </View>
      </View>
      <LoginBtn onPress={handleAuth} /> 
    </View>
  );
}

const { width } = Dimensions.get("window")

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center'
  },
  icons: {
    color: "#fff",
    fontSize: 100
  },
  container_2: {
    flexGrow: 1,
    width,
    justifyContent: 'center',
    alignItems: 'center'

  },
  titleContainer: {
    width: width - 80,
    height: 50,
    justifyContent: 'center',
    alignItems: 'flex-start'
  }
});
