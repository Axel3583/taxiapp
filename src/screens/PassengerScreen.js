import { StyleSheet, Dimensions, View, ActivityIndicator, Text, TouchableWithoutFeedback } from 'react-native';
import React, { useState, useEffect, useRef } from 'react';
import * as Location from 'expo-location';
import PlaceInput from '../components/PlaceInput';
import { BASE_URL, API_KEY, getRoute, decodePoint } from '../../utils/prefix';
// import MapView, { Polyline, Marker } from 'react-native-maps';



export default function PassengerScreen() {

    const mapView = useRef();
    const initialState = { latitude: null, longitude: null, coordinates: [], destinationCoords: null }
    const [state, setState] = useState(initialState)
    const { latitude, longitude, coordinates, destinationCoords } = state

    const getUserLocation = async () => {
        try {
            const {
                coords: { latitude, longitude }
            } = await Location.getCurrentPositionAsync();
            console.log('location', latitude, longitude);
            setState(prevState => ({
                ...prevState,
                latitude,
                longitude
            }))
        } catch (error) {
            console.error('Localisation error :', error);
        }
    }

    useEffect(() => {
        getUserLocation();
    }, [])

    if (!longitude || !latitude) {
        return (
            <View style={styles.container}>
                <ActivityIndicator size={'large'} />
            </View>
        )
    }

    const handlePredictionPress = async (place_id) => {
        try {
            const url = `${BASE_URL}/directions/json?key=${API_KEY}&destination=place_id:${place_id}&origin=${latitude}, ${longitude}`;
            // console.log('url:', url);
            const points = await getRoute(url);

            console.log('points :', points);

            const coordinates = decodePoint(points);
            setState(prevState => ({
                ...prevState,
                coordinates,
                destinationCoords: coordinates[coordinates.length - 1]
            }));

            mapView.current.fitToCoordinates(coordinates, {
                animated: true,
                edgepadding: {
                    top: 110,
                    bottom: 40,
                    left: 40,
                    right: 40
                }
            });
        } catch (error) {
            console.log('Erreur prediction press', error);
        }
    }

    return (
        <TouchableWithoutFeedback>
            <View style={styles.container}>

                <Text>Passenger Screen</Text>

                {/* <MapView
                    ref={mapView}
                    style={styles.map}
                    showUserLocation
                    followUserLocation
                    region={
                        {
                            latitude,
                            longitude,
                            latitudeDelta: 0.015,
                            longitudeDelta: 0.121
                        }
                    }>
                    {coordinates.length > 0 && (
                        <Polyline
                            coordinates={coordinates}
                            strokeWidth={7}
                            strokeColor='2dbb54'
                        />
                    )}
                    {destinationCoords && (<Marker
                        cordinates={destinationCoords}
                    />)}
                </MapView> */}
                <PlaceInput
                    latitude={latitude}
                    longitude={longitude}
                    onPredictionPress={handlePredictionPress}
                   /> 
                    {/* onPress={() => Keyboard.dismiss()}  */}

            </View>
        </TouchableWithoutFeedback>
    )
}

const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: '#fff',
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    map: {
        height,
        width
    }
})
