import React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import Block from '../components/Block';
import Title from '../components/Title';
import { Ionicons } from '@expo/vector-icons';
import { prefix } from '../../utils/prefix';
import RoundBtn from '../components/RoundBtn';

export default function HomeScreen({ navigation }) {

    // const onPress = () => {
    //     navigation.navigate('Passenger')
    // }
    // const goTo = () => {
    //     navigation.navigate('Driver')
    // }

    // const goToFood = () => {
    //     navigation.navigate('Food')
    // }

    return (
        <View style={styles.container}>
            <Block>
                <Ionicons name={`${prefix}-car-sport-outline`} style={styles.icons} />
                <Title size={'big'} content="TAXI APP" />
            </Block>

            <View style={styles.container_2}>
                <View style={styles.titleContainer}>
                    <Title size={'small'} content="Bienvenue" />
                    <Title size={'medium'} content="Vous recherchez ?" />
                </View>
            </View>

            {/* <View style={styles.containerBtn}>
                <RoundBtn iconName={`${prefix}-person`} onPress={goTo} />
                <RoundBtn iconName={`${prefix}-fast-food-outline`} onPress={goToFood} />
                <RoundBtn iconName={`${prefix}-car-sport-outline`} onPress={onPress} />
            </View> */}

        </View>
    );
}

const { width } = Dimensions.get("window")

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        alignItems: 'center'
    },
    icons: {
        color: "#fff",
        fontSize: 100
    },
    container_2: {
        flexGrow: 1,
        width,
        justifyContent: 'center',
        alignItems: 'center'

    },
    titleContainer: {
        width: width - 80,
        height: 50,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    containerBtn: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: width - 80,
        alignItems: 'center'
    }
});
