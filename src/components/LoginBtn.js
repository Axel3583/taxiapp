import React from "react";
import { StyleSheet, TouchableOpacity, View, Text, Image, Dimensions } from 'react-native';
import Logo from '../../assets/9/images/google.png';
import Title from "./Title";

export default function LoginBtn({ onPress }) {

    return (
        <TouchableOpacity onPress={onPress} style={{ marginBottom: 30, height: 50}}>
            <View style={styles.container}>
                <Title size={'small'} content="Connexion google" />
                <Image style={styles.img} source={Logo} />
            </View>

        </TouchableOpacity>
    )

}

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        width: width - 80,
        height: 55,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowColor: '#000',
        shadowRadius: 4,
        elevation: 5,
        borderRadius: 10,
        backgroundColor: "#fff"
    },
    img: {
        width: 40,
        height: 40,
        marginRight: 20
    }
})