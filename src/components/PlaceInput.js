import { Ionicons } from "@expo/vector-icons/build/Icons";
import React, { useState } from "react";
import { Dimensions, StyleSheet, TextInput, View, ActivityIndicator } from 'react-native';
import { prefix, BASE_URL, API_KEY } from "../../utils/prefix";
import axios from 'axios';
import Prediction from "./Predictions";

export default function PlaceInput({ latitude, longitude, onPredictionPress }) {

    const initialState = {
        place: "",
        prediction: [],
        loading: false
    }

    const [state, setState] = useState(initialState);
    const { place, loading, prediction } = state;


    const search = async url => {
        try {
            const { data } = await axios.get(url);
            setState(prevState => ({
                ...prevState,
                prediction: data.predictions,
                loading: false
            }));
        } catch (error) {
            console.error('error search', error);
        }
    }

    const handleChangeText = (value) => {
        setState(prevState => ({
            ...prevState,
            place: value,
            loading: true
        }));
        const url = `${BASE_URL}/place/autocomplete/json?key=${API_KEY}&input=${value}&location=${latitude},${longitude}&radius=2000&language=fr`;
        // const url = `https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${API_KEY}&input=${value}&location=${latitude},${longitude}&radius=2000&language=fr`;
        console.log(url, 'url');
        search(url);
    }

    const renderPrediction = () => {
        return prediction.map(prediction => {
            const { structured_formatting, reference, place_id, description } = prediction;
            return (
                <Prediction
                    main_text={structured_formatting.main_text}
                    secondary_text={description}
                    key={reference}
                    onPress={() => {
                        onPredictionPress(place_id);
                        setState(prevState => ({
                            ...prevState,
                            prediction: [],
                            place: structured_formatting.main_text
                        }))
                    }}
                />
            )
        })
    }
    return (
        <View style={styles.container}>
            <View style={styles.InputContainer}>
                {/* <TextInput
                    style={styles.input}
                    value={place}
                    onChangeText={handleChangeText} /> */}

                <TextInput
                    editable
                    style={styles.input}
                    onChangeText={handleChangeText}
                    value={place}
                    placeholder="search"
                    keyboardType="numeric"
                />

                {!loading && (<Ionicons style={styles.icon} name={`${prefix}-search`} />)}
                {loading && <ActivityIndicator />}

            </View>
            {!loading && prediction.length > 0 ? renderPrediction() : console.log(prediction, 'preidc')}
        </View>
    )
}

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        position: "absolute",
        width: width - 50,
        top: 20,
        paddingHorizontal: 5,
        backgroundColor: "#fff",
        borderRadius: 8,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowColor: '#000',
        shadowRadius: 3.84,
        shadowOpacity: 0.25,
        elevation: 5
    },
    icon: {
        fontSize: 25,
        color: "#d6d6d6"
    },
    input: {
        fontSize: 20,
        height: 40,
        color: "#303030",
        maxWidth: "100%",
        minWidth: "90%",
        fontFamily: "Poppins"
    },
    InputContainer: {
        height: "40%",
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10,
        paddingHorizontal: 10,
        borderColor: 'gray',
    }
})