import { Ionicons } from "@expo/vector-icons";
import React from "react";
import { StyleSheet, TouchableOpacity, View } from 'react-native';


export default function RoundBtn({iconName, onPress}){


  return(
    <TouchableOpacity onPress={onPress}>
        <View style={styles.container}>
            <Ionicons style={styles.icon}  name={iconName} />
        </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
    container: {
     alignItems: 'center',
     backgroundColor: "#2dbb54",
     borderRadius: 40,
     justifyContent: 'center',
     height: 85,
     width: 85
    
    },
    icon: {
       fontSize: 45,
       color: 'white'
    }
})