import React from "react";
import { StyleSheet, View, Dimensions } from 'react-native';

export default function Block({ children }) {

    return <View style={styles.container}>{ children }</View>
}

const { width } = Dimensions.get("window")

const styles = StyleSheet.create({
    container: {
        width,
        backgroundColor: "#2dbb54",
        flexGrow: 3,
        borderBottomLeftRadius: width,
        borderBottomRightRadius: width,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.25,
    }
})