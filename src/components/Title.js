import React from "react";
import { StyleSheet, View, Text } from 'react-native';

export default function Title({content, size}) {

    const { container, big, small, medium } = styles

    const goToStyles = () => {
        switch (size) {
            case 'big':
                return big;
            case 'small':
                return small;
            case 'medium':
                return medium;
        }
    }

    return (
        <View style={container}>
            <Text style={goToStyles()}>{ content }</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        maxHeight: 90
    },
    big: {
        color: 'white',
        fontSize: 35,
        fontWeight: "bold",
        fontFamily: 'LeckerliOne'
    },
    small: {
        color: "rgba(0,0,0,0,6)",
        fontFamily: "Poppins",
        fontSize: 12,
        fontWeight: "700",
        lineHeight: 28
    },
    medium: {
        fontFamily: "Poppins",
        fontSize: 24,
        fontWeight: "bold",
        lineHeight: 28
    }
})