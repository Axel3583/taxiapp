import React from "react";
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';



export default function Prediction({main_text, secondary_text, onPress}){

  return(
    <TouchableOpacity onPress={onPress}>
     <View style={styles.container}>
        <Text numberOfLines={1} style={styles.secondary}>{secondary_text}</Text>
        <Text style={styles.main}>{main_text}</Text>
    </View>   
    </TouchableOpacity>
  )

}

const styles = StyleSheet.create({
    container: {
      width: "100%",
      borderTopWidth: 1,
      borderColor: "f6f6f6",
      padding: 5
    },

    secondary: {
     color: "#d6d6d6",
     fontSize: 12,
     fontWeight: "300",
     fontFamily: "Poppins"
    },
    main: {
        color: "#303030",
        fontSize: 16,
        fontWeight: "700",
        fontFamily: "Poppins"
       }
})