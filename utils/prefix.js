import { Platform, AsyncStorage } from "react-native";
import * as Google from 'expo-auth-session/providers/google';
import axios from "axios";
import PolyLine from '@mapbox/polyline'


export const prefix = Platform.OS === "ios" ? "ios" : "md";

export const config = {
    iosClientId: '5829102384-0j9u18a1hbusglpul7ihg9gg3f3q2fce.apps.googleusercontent.com',
    androidClientId: '5829102384-3jfr27v7jecojt8cc6k7ssft901d784c.apps.googleusercontent.com',
    webClientId: '5829102384-l90othq4rpt4dcbmmqea3pooofpkhedv.apps.googleusercontent.com'
};

// export const useGoogleAuth = async ({ user, type }) => {

//     if (type === 'sucess') {
//         const { name, photoUrl, email } = user;
//         await AsyncStorage.setItem('user', JSON.stringify({
//             name,
//             photoUrl,
//             email
//         }),
//         )
//     }
//     const [request, response, promptAsync] = Google.useAuthRequest({

//         clientId: Platform.OS === 'android' ? config.androidClientId : config.iosClientId,
//         // This is the endpoint that Google will redirect the user to after they have authenticated
//         redirectUri: 'http://localhost:19006/',
//         // This tells Google what you want to access (i.e. the user's email and basic profile)
//         scopes: ['openid', 'profile', 'email'],
//     });

//     return { request, response, promptAsync };
// };


// export const API_KEY = "AIzaSyDBYzBUZARECIlqnaox2HCoviCO0hqwo5s"; //Restreint
export const API_KEY = "AIzaSyD_8foHs0-gIFZ_VQOfJ74loKcAJZgE82s"; // Libre

export const BASE_URL = "https://maps.googleapis.com/maps/api"

export const renderInitialScreen = () => {
    try {
        // let user = AsyncStorage.getItem('user');
        let user = 'user'
        // JSON.parse(user)
        JSON.stringify(user)
        return user ? 'Home' : 'Login'

    } catch (error) {
        console.log('erreur :', error);
    }
}

// Fonction pour tracer une routes sur La carte
export const getRoute = async (url) => {
    try {
      const {
        data: {routes}
      } = await axios.get(url)
      const points = routes[0].overview_polyline.points;
      return points;
    } catch (error) {
        console.log('Error routes : ', error);
    }
}

export const decodePoint = () => {
    const fixPoints = PolyLine.decode(point);
    
    const route = fixPoints.map(fixPoint => {
        return {
            latitude: fixPoint[0],
            longitute: fixPoint[1]
        }
    });
    console.log('route', route);
    return route;
}
